// 배송비 계산 기능 추가 - 시작
add_action('after_setup_theme', 'apmmust_add_shipping_fee_option_page');
function apmmust_add_shipping_fee_option_page()
{
  if (function_exists('acf_add_options_page')) {
    acf_add_options_page(array(
      'page_title'    => '배송비 관리',
      'menu_title'    => '배송비 관리',
      'menu_slug'     => 'global-shopping-fee-settings',
      'capability'    => 'edit_posts',
      'redirect'      => false
    ));
  }
}


add_action('wp_enqueue_scripts', 'apmmust_enqueue_scripts', 10);
function apmmust_enqueue_scripts()
{
  wp_enqueue_script('the-apmmust-main-js', get_stylesheet_directory_uri() . '/main.js', array('jquery'), '1.0.0', true);
  wp_localize_script('the-apmmust-main-js', 'apmmust_ajax_obj', array('ajaxurl' => admin_url('admin-ajax.php')));
}

add_shortcode('shipping_calculator', 'apmmust_shipping_calculator');
function apmmust_shipping_calculator($attrs)
{
  if (function_exists('acf_add_options_page')) {
    ob_start();
    get_template_part('shipping-calculator');
    return ob_get_clean();
  }
  return 'ACF Pro 플러그인이 설치되어 있지 않습니다';
}

function apmmust_get_shipping_fields()
{
  $page_slug = 'option';
  $repeater_field_name = 'my_global_shipping_fee_setting_repeater';
  $country_field_name = 'country';
  $shipping_type_field_name = 'shipping_type';
  $estimate_shpping_date = 'estimate_shipping_date';
  $shipping_fee_kg_field_name = 'shipping_fee_kg';

  $rows = [];
  if (have_rows($repeater_field_name, $page_slug)) :
    while (have_rows($repeater_field_name, $page_slug)) : the_row();
      $rows[] = array(
        'country' => get_sub_field($country_field_name),
        'shipping_type' => get_sub_field($shipping_type_field_name),
        'estimate_shipping_date' => get_sub_field($estimate_shpping_date),
        'shipping_fee_kg' => array_reduce(get_sub_field($shipping_fee_kg_field_name), function ($acc, $cur) {
          if ($acc === null) $acc = [];
          return array_merge($acc, $cur);
        }),
      );
    endwhile;
  endif;

  do_action('qm/debug', $rows);

  return $rows;
}

function apmmust_get_shipping_box_dimensions()
{
  $page_slug = 'option';
  $box_dimension_field_name = 'box_dimension';
  $box_dimension_width_field_name = 'width';
  $box_dimension_height_field_name = 'height';
  $box_dimension_depth_field_name = 'depth';

  $fomula = apmmust_get_weight_fomula();

  $box_dimensions = [];
  if (have_rows($box_dimension_field_name, $page_slug)) :
    while (have_rows($box_dimension_field_name, $page_slug)) : the_row();
      $row = array(
        'width' => get_sub_field($box_dimension_width_field_name),
        'height' => get_sub_field($box_dimension_height_field_name),
        'depth' => get_sub_field($box_dimension_depth_field_name),
      );

      $multiple_result = intval($row['width']) * intval($row['height']) * intval($row['depth']);

      $row['weight'] = array(
        'ems' => ceil($multiple_result / $fomula['ems']),
        'ups' => ceil($multiple_result / $fomula['ups']),
      );

      $box_dimensions[] = $row;
    endwhile;
  endif;

  return $box_dimensions;
}

function apmmust_get_weight_fomula()
{
  $result = get_field('kg_fomula', 'option');
  return array(
    "ems" => isset($result['ems']) ? intval($result['ems']) : 6000,
    "ups" => isset($result['ups']) ? intval($result['ups']) : 6000,
  );
}

function apmmust_get_possible_shipping_type_of_country(&$rows)
{
  $country_possible_shipping_type_map = [];
  foreach ($rows as $row) {
    $country = $row['country'];
    if (!isset($country_possible_shipping_type_map[$country])) {
      $country_possible_shipping_type_map[$country] = [];
    }
    $country_possible_shipping_type_map[$country][] = $row['shipping_type'];
  }
  return $country_possible_shipping_type_map;
}

add_action('wp_ajax_apmmust_calculate_shipping_fee_action', 'apmmust_calculate_shipping_fee_action');
add_action('wp_ajax_nopriv_apmmust_calculate_shipping_fee_action', 'apmmust_calculate_shipping_fee_action');
function apmmust_calculate_shipping_fee_action()
{
  if (!isset($_POST['country']) || !isset($_POST['shipping_type']) || !isset($_POST['weight'])) {
    wp_send_json_error(array(
      "code" => 401,
      "message" => 'no value'
    ), 401);
    return;
  }

  $country = $_POST['country'];
  $shipping_type = $_POST['shipping_type'];
  $weight = $_POST['weight'];

  if (empty($country) || empty($shipping_type) || empty($weight)) {
    wp_send_json_error(array(
      "code" => 403,
      "message" => 'empty value'
    ));
    return;
  }

  if ($shipping_type !== 'ems' && $shipping_type !== 'ups') {
    wp_send_json_error(array(
      "code" => 402,
      "message" => 'invalid value'
    ));
    return;
  }

  $weight = intval($weight);

  if ($weight <= 0) {
    wp_send_json_error(array(
      "code" => 402,
      "message" => 'invalid value'
    ));
    return;
  }

  $rows = apmmust_get_shipping_fields();
  $fomula = apmmust_get_weight_fomula()[$shipping_type];

  $divisor = 30;

  // 가능한 운송 수단을 선택했는지 점검
  $country_possible_shipping_type_map = apmmust_get_possible_shipping_type_of_country($rows);
  if (!in_array($shipping_type, $country_possible_shipping_type_map[$country])) {
    $type = strtoupper($shipping_type);
    wp_send_json_error(array(
      'code' => 401,
      'message' => "{$type} is not supported at the country",
    ));
    return;
  }

  for ($i = 0; $i < count($rows); $i += 1) {
    $row = $rows[$i];

    if ($country === $row['country'] && $shipping_type === $row['shipping_type']) {
      $quotient = intdiv($weight, $divisor);
      $remainder = $weight % $divisor;

      // 우선 30kg을 초과했다면 이래나 저래나 30kg중량으로 계산한다
      $price = $quotient * intval($row['shipping_fee_kg']["shipping_fee_kg_30"]);

      if ($remainder > 0) {
        // 30으로 나눈 나머지와 부피중량중 더 큰값을 사용한다
        $box_dimensions = apmmust_get_shipping_box_dimensions();

        $max_weight = array_reduce(array_reverse($box_dimensions), function ($acc, $cur) use ($shipping_type, $remainder) {
          $w = intval($cur['weight'][$shipping_type]);
          if ($remainder <= $w) {
            return $w;
          }
          return $acc;
        }, $divisor);

        $price += intval($row['shipping_fee_kg']["shipping_fee_kg_{$max_weight}"]);
      }

      $data = WOOMULTI_CURRENCY_Data::get_ins();
      $rates = $data->get_exchange('USD', 'KRW');

      $paypal_fee = 1.043;
      $usd = number_format(($price / $rates['KRW']) * $paypal_fee, 2); // paypal 수수료 적용

      $result = array(
        'price_krw' => $price,
        'weight' => $weight,
        'price_usd' => $usd,
        'estimate_date' => intval($row['estimate_shipping_date']),
        'rate_krw' => $rates['KRW'],
      );

      wp_send_json_success($result);
      return;
    }
  }
}

add_action('wp_ajax_apmmust_shipping_fee_table_action', 'apmmust_shipping_fee_table_action');
add_action('wp_ajax_nopriv_apmmust_shipping_fee_table_action', 'apmmust_shipping_fee_table_action');
function apmmust_shipping_fee_table_action()
{
  if (!isset($_POST['country']) || !isset($_POST['shipping_type'])) {
    wp_send_json_error(array(
      "code" => 404,
      "message" => 'no value'
    ));
    return;
  }

  $country = $_POST['country'];
  $shipping_type = $_POST['shipping_type'];

  if (empty($country) || empty($shipping_type)) {
    wp_send_json_error(array(
      "code" => 403,
      "message" => 'empty value'
    ), 401);
    return;
  }

  if ($shipping_type !== 'ems' && $shipping_type !== 'ups') {
    wp_send_json_error(array(
      "code" => 402,
      "message" => 'invalid value'
    ), 401);
    return;
  }

  $rows = apmmust_get_shipping_fields();

  // 가능한 운송 수단을 선택했는지 점검
  $country_possible_shipping_type_map = apmmust_get_possible_shipping_type_of_country($rows);
  if (!in_array($shipping_type, $country_possible_shipping_type_map[$country])) {
    $type = strtoupper($shipping_type);
    wp_send_json_error(array(
      'code' => 401,
      'message' => "{$type} is not supported at the country",
    ));
    return;
  }

  for ($i = 0; $i < count($rows); $i += 1) {
    $row = $rows[$i];

    if ($country === $row['country'] && $shipping_type === $row['shipping_type']) {
      $data = WOOMULTI_CURRENCY_Data::get_ins();
      $rates = $data->get_exchange('USD', 'KRW');

      $paypal_fee = 1.043;
      $shipping_fee_table = [];
      foreach ($row['shipping_fee_kg'] as $key => $price) {
        $usd = number_format(($price / $rates['KRW']) * $paypal_fee, 2); // paypal 수수료 적용
        $shipping_fee_table[] = $usd;
      }

      $result = array(
        'estimate_date' => intval($row['estimate_shipping_date']),
        'rate_krw' => $rates['KRW'],
        'table' => $shipping_fee_table
      );

      wp_send_json_success($result);
      return;
    }
  }
}
// 배송비 계산 기능 추가 - 끝
