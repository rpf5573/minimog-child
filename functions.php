<?php
defined('ABSPATH') || exit;

/**
 * Enqueue child scripts
 */
if (!function_exists('minimog_child_enqueue_scripts')) {
  function minimog_child_enqueue_scripts()
  {
    wp_enqueue_style('minimog-child-style', get_stylesheet_directory_uri() . '/style.css', array(), '1.0.1');
    wp_enqueue_style('minimog-child-style-custom', get_stylesheet_directory_uri() . '/custom-style.css', array(), '1.0.2');
  }
}
add_action('wp_enqueue_scripts', 'minimog_child_enqueue_scripts', 15);

add_action('admin_enqueue_scripts', 'apmmust_admin_style', 99);
function apmmust_admin_style()
{
  wp_enqueue_style('apmmust_admin_style', get_stylesheet_directory_uri() . '/admin-style.css');
}

//Search Engines에서 상품 가격 숨기기
add_filter('woocommerce_structured_data_product_offer', '__return_empty_array');
// Order View 페이지에서 상품 썸네일 표시
add_filter('woocommerce_order_item_name', 'display_product_image_in_order_item', 20, 3);
function display_product_image_in_order_item($item_name, $item, $is_visible)
{
  // Targeting view order pages only
  if (is_wc_endpoint_url('view-order')) {
    if ($item->get_product()) {
      $product = $item->get_product(); // Get the WC_Product object (from order item)
      $thumbnail = $product->get_image(array(80, 80)); // Get the product thumbnail (from product object)
      if ($product->get_image_id() > 0)
        $item_name = '<div class="item-thumbnail">' . $thumbnail . '</div>' . $item_name;
    } else {
      $item_name = '<div class="item-thumbnail"><img width="80" height="80" src="' . wc_placeholder_img_src() . '" class="attachment-80x80 size-80x80" alt="" loading="lazy"></div>' . $item_name;
    }
  }
  return $item_name;
}
//썸네일 자동생성 
add_filter('woocommerce_background_image_regeneration', '__return_false');

//품절 상품 카트에서 자동 삭제
function ced_out_of_stock_products()
{
  if (WC()->cart->is_empty()) {
    return;
  }

  $removed_products = [];

  foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
    $product_obj = $cart_item['data'];

    if (!$product_obj->is_in_stock()) {
      WC()->cart->remove_cart_item($cart_item_key);
      $removed_products[] = $product_obj;
    }
  }

  if (!empty($removed_products)) {
    wc_clear_notices();
    foreach ($removed_products as $idx => $product_obj) {
      $product_name = $product_obj->get_title();

      //your notice here
      $msg = sprintf(__("The product '%s' was removed from your cart because it is out of stock.", 'woocommerce'), $product_name);

      wc_add_notice($msg, 'error');
    }
  }
}
add_action('woocommerce_before_cart', 'ced_out_of_stock_products');

// Action Scheduler 로그 파일 매일 Purge
add_filter('action_scheduler_retention_period', 'wpb_action_scheduler_purge');
function wpb_action_scheduler_purge()
{
  return HOUR_IN_SECONDS;
}

//우커머스 로그인 에러 문제 해결 *작동 여부 미확인
add_filter('nonce_user_logged_out', function ($uid, $action) {
  if ($uid && $uid != 0 && $action && $action == 'woocommerce-login') {
    $uid = 0;
  }
  return $uid;
}, 100, 2);

// brand taxonomy에 hierarchical 옵션을 설정하는 코드
// 상위 브랜드를 설정할 수 있게됩니다
// 상위 브랜드 설정 코드 - 시작
add_filter('register_product_brand_taxonomy_args', 'apmmust_change_product_brand_taxonomy_args');
function apmmust_change_product_brand_taxonomy_args($args)
{
  $args['hierarchical'] = true;
  return $args;
}
// 상위 브랜드 설정 코드 - 끝

// 로그인 하지 않은 유저는 홈으로 돌려보낸다 - 시작
function apmmust_disable_search_for_guests()
{
  if (!is_user_logged_in() && is_search()) {
    $my_account_url = get_permalink(get_option('woocommerce_myaccount_page_id'));
    wp_redirect($my_account_url);
    exit;
  }
}
add_action('template_redirect', 'apmmust_disable_search_for_guests');
// 로그인 하지 않은 유저는 홈으로 돌려보낸다 - 끝

// 로그인 하지 않은 유저들은 preview를 볼 수 없다 - 시작
// add_action( 'wp_ajax_minimog_search_products', 'minimog_prevent_show_preview' );
add_action('wp_ajax_nopriv_minimog_search_products', 'minimog_prevent_show_preview_for_guests', -1);
function minimog_prevent_show_preview_for_guests()
{
  $my_account_url = get_permalink(get_option('woocommerce_myaccount_page_id'));
  wp_send_json_success([
    'template' => '<p style="height: 60px;" class="woocommerce-info">Please <a href="' . $my_account_url . '">login</a> to access apM MUST.</p><style>.popup-search-results .minimog-grid.lazy-grid.loaded { height: 80px !important; }</style>',
  ]);
  exit;
}
// 로그인 하지 않은 유저들은 preview를 볼 수 없다 - 끝

//** Product Custom Fields for Korean name and Wholesale Price **//
// Display Fields
add_action('woocommerce_product_options_general_product_data', 'woocommerce_product_custom_fields');
// Save Fields
add_action('woocommerce_process_product_meta', 'woocommerce_product_custom_fields_save');
function woocommerce_product_custom_fields()
{
  global $woocommerce, $post;
  echo '<div class="product_custom_field">';
  // Custom Product Text Field
  woocommerce_wp_text_input(
    array(
      'id' => '_custom_product_korean_name',
      'placeholder' => '',
      'label' => __('Korean Name', 'woocommerce'),
      'desc_tip' => 'true'
    )
  );
  //Custom Product Number Field
  woocommerce_wp_text_input(
    array(
      'id' => '_custom_product_wholesale_price',
      'placeholder' => '',
      'label' => __('Wholesale Price', 'woocommerce'),
      'type' => 'number',
      'custom_attributes' => array(
        'step' => 'any',
        'min' => '0'
      )
    )
  );
  echo '</div>';
}

function woocommerce_product_custom_fields_save($post_id)
{
  // Custom Product Text Field
  $woocommerce_custom_product_text_field = $_POST['_custom_product_korean_name'];
  if (!empty($woocommerce_custom_product_text_field))
    update_post_meta($post_id, '_custom_product_korean_name', esc_attr($woocommerce_custom_product_text_field));
  // Custom Product Number Field
  $woocommerce_custom_product_number_field = $_POST['_custom_product_wholesale_price'];
  if (!empty($woocommerce_custom_product_number_field))
    update_post_meta($post_id, '_custom_product_wholesale_price', esc_attr($woocommerce_custom_product_number_field));
}

//* Done


// 검색 결과에 재고 없는 상품은 안보여지도록 - 시작
add_action('pre_get_posts', 'apmmust_exclude_product_by_search');
function apmmust_exclude_product_by_search($query)
{
  if (!is_admin() && $query->is_search()) {
    $meta_query = $query->get('meta_query');
    if (!is_array($meta_query)) {
      $meta_query = array();
    }

    $meta_query[] = array(
      'key' => '_stock_status',
      'value' => 'onbackorder',
      'compare' => 'NOT LIKE'
    );
    $query->set('meta_query', $meta_query);
  }
}
// 검색 결과에 재고 없는 상품은 안보여지도록 - 끝


// 인피니트 스크롤 리스폰스 (기존에는 페이지 전체를 불러왔는데, 이제는 일부분만 돌려준다)
add_action('wp_ajax_apmmust_get_next_products_by_filter', 'apmmust_get_next_products_by_filter_callback');
add_action('wp_ajax_nopriv_apmmust_get_next_products_by_filter', 'apmmust_get_next_products_by_filter_callback');
function apmmust_get_next_products_by_filter_callback()
{
  global $wp_query;

  if (!isset($_POST['full_url']) || empty($_POST['full_url'])) {
    wp_send_json_error('Missing full_url parameter!');
    wp_die();
  }
  if (!isset($_POST['wp_query']) || empty($_POST['wp_query'])) {
    wp_send_json_error('Missing wp_query parameter!');
    wp_die();
  }
  // empty검사는 안한다. 0일수도 있으니
  if (!isset($_POST['current_page'])) {
    wp_send_json_error('Missing current_page parameter!');
    wp_die();
  }

  $current_page = intval($_POST['current_page']);

  // escape " 를 삭제하자
  $pre_wp_query = unserialize(stripslashes($_POST['wp_query']));
  if (!$pre_wp_query) {
    wp_send_json_error('Invalid wp_query parameter!');
    wp_die();
  }
  $query_vars = $pre_wp_query['query_vars'];
  $query_vars['paged'] = $current_page;

  $query = new WP_Query($query_vars);

  $total = $query->max_num_pages;
  $current = $query->query_vars['paged'];

  ob_start();

  if ($query->have_posts()) { ?>
    <div id="minimog-main-post" data-current-page="<?php echo $query_vars['paged']; ?>">
      <?php
      while ($query->have_posts()) {
        $query->the_post();
        minimog_get_wc_template_part('content-product', '01', [
          'settings' => [
            'show_list_view' => true,
          ],
        ]);
      }

      // pagination도 뿌려준다
      \Minimog_Woo::instance()->get_pagination(array('total' => $total, 'current' => $current));
      ?>
    </div>
    <?php
  }
  echo ob_get_clean();

  wp_die();
}

// 상품 variation관계없이 같은 상품이면 그룹핑하고, 갯수도 알려준다
function apmmust_group_products_that_are_the_same_regardless_of_product_variation_and_count($cart_items)
{
  $products = array();
  foreach ($cart_items as $cart_item) {
    $product = $cart_item['data'];
    $product_id = $cart_item['product_id'];
    $product_name = $product->get_name();
    if (!isset($products[$product_id])) {
      $products[$product_id] = array(
        'count' => 0,
        'name' => $product_name,
      );
    }
    $products[$product_id]['count'] += intval($cart_item['quantity']);
  }
  return $products;
}

function apmmust_get_initial_notices()
{
  $notices = WC()->session->get(
    'wc_notices',
    array(
      'error' => array()
    )
  );
  $error_notices = array();
  foreach ($notices['error'] as $index => $notice) {
    if (!isset($notice['data']) || $notice['data'] !== 'apmmust_error') { // 기존의 에러들은 보존한다
      $error_notices[] = $notice;
    }
  }
  $notices['error'] = $error_notices;
  return $notices;
}

function apmmust_set_notice_error($notices, $message)
{
  $notices['error'][] = array(
    'notice' => $message,
    'data' => 'apmmust_error'
  );
  return $notices;
}

function apmmust_update_notice_error($cart_items = array())
{
  $products = apmmust_group_products_that_are_the_same_regardless_of_product_variation_and_count($cart_items);
  $notices = apmmust_get_initial_notices();

  foreach ($products as $product_id => $product) {
    // 2개 미만이면 무조건 에러
    if ($product['count'] < 2) {
      $message = "{$product['name']} - You must purchase a minimum of two products.";
      $notices = apmmust_set_notice_error($notices, $message);
    }
  }

  WC()->session->set('wc_notices', $notices);
}

// 상품 상세페이지에서 상품을 담을때 종류상관없이 최소 2개를 담지 않으면 오류 메세지를 띄운다
add_filter('woocommerce_add_to_cart_validation', function ($passed, $product_id, $quantity) {
  // 2개를 넘더라도 우리가 session으로 관리하고 있으니까 여기서도 초기화 해줘야한다
  $cart_items = WC()->cart->get_cart();
  $products = apmmust_group_products_that_are_the_same_regardless_of_product_variation_and_count($cart_items);
  $product = wc_get_product($product_id);
  $products[$product_id]['name'] = $product->get_name();
  $products[$product_id]['count'] += $quantity;

  WC()->session->set('wc_notices', []); // 초기화 안해주면 다른 에러도 같이 뜬다

  if ($products[$product_id]['count'] < 2) {
    $product_name = $products[$product_id]['name'];
    $message = "{$product_name} - You must purchase a minimum of two products. ";
    $notices = array(
      'error' => array(
        array(
          'notice' => $message,
          'data' => 'apmmust_error'
        )
      )
    );
    WC()->session->set('wc_notices', $notices);
  }
  return $passed;
}, 9999, 3); // B2B King이 뭘할지 모르니까 우선순위를 낮게 잡는다


// 상품 갯수를 업데이트하면 에러 메세지를 삭제한다
// 주의: 상품 상세페이지에서도 호출된다. cart에서만 호출되도록 수정해야함
add_action('woocommerce_after_cart_item_quantity_update', function ($cart_item_key, $quantity, $old_quantity, $cart) {
  // 카트페이지가 아니라 일반 상품 상세페이지에서도 호출된다
  // 그래서 상품상세페이지라면 아래 코드를 처리하지 않는다
  if (!is_cart())
    return;

  $cart_items = $cart->get_cart();
  apmmust_update_notice_error($cart_items);
}, 9999999999, 4);

// Cart에서 error를 보여준다
add_action('woocommerce_check_cart_items', function () {
  $cart_items = WC()->cart->get_cart();
  apmmust_update_notice_error($cart_items);
});

add_action('woocommerce_cart_item_removed', function ($cart_item_key, $cart) {
  apmmust_update_notice_error($cart->get_cart());
}, 10, 2);

// checkout 페이지에서 Place older버튼대신 Cart로 이동시키는 버튼으로 대체한다
add_action('woocommerce_order_button_html', function ($button_html) {
  $cart_items = WC()->cart->get_cart();
  $products = apmmust_group_products_that_are_the_same_regardless_of_product_variation_and_count($cart_items);
  foreach ($products as $product_id => $product) {
    // 2개 미만이면 무조건 에러
    if ($product['count'] < 2) {
      $class_names = ' button alt ' . esc_attr(wc_wp_theme_get_element_class_name('button') ? ' ' . wc_wp_theme_get_element_class_name('button') : '');
      $button = '<a href="' . wc_get_cart_url() . '" class="' . $class_names . '">' . 'Go To Cart' . '</a>';
      ob_start();
      ?>

      <div>
        <p>
          There are problems with your cart. Check please.
        </p>
        <?php
        echo $button;
        ?>
      </div>

      <?php
      return ob_get_clean();
    }
  }

  return $button_html;
}, 9999999999);

// 사용자가 어떤 비밀스러운 방법을 사용해서 주문을 하려고 해도, 막는다
add_action('woocommerce_before_checkout_process', function () {
  $cart_items = WC()->cart->get_cart();
  $products = apmmust_group_products_that_are_the_same_regardless_of_product_variation_and_count($cart_items);

  foreach ($products as $product_id => $product) {
    // 2개 미만이면 무조건 에러
    if ($product['count'] < 2) {
      throw new Exception("You cannot process checkout. Please check your cart again.");
    }
  }
});

// Add custom CSS to WooCommerce emails
function custom_woocommerce_email_styles($css)
{
  $custom_css = '
        #template_header_image img {
            float: right;
            width: 200px;
        }
    ';

  return $css . $custom_css;
}
add_filter('woocommerce_email_styles', 'custom_woocommerce_email_styles', 999, 1);

// Regenerate the product attributes lookup table 사이즈 증가 //
add_filter('woocommerce_attribute_lookup_regeneration_step_size', function () {
  return 5000;
});

// product category에 HS_Code추가하고,
// pdf invoice에 추가로 값넣기
add_filter('rwmb_meta_boxes', 'apmmust_hscode_field_in_product_category');
function apmmust_hscode_field_in_product_category($meta_boxes)
{
  $prefix = '';

  $meta_boxes[] = [
    'title' => __('HS_Code', 'apmmust'),
    'id' => 'hs_code',
    'taxonomies' => ['product_cat'],
    'fields' => [
      [
        'name' => __('HSCode', 'apmmust'),
        'id' => $prefix . 'hscode_text',
        'type' => 'text',
      ],
    ],
  ];

  return $meta_boxes;
}

add_action('wpo_wcpdf_after_item_meta', function ($document_type, $item, $document_order) {
  if (empty($item['product']))
    return;
  if (empty($item['product_id']))
    return;

  $product_id = $item['product_id'];
  $terms = get_the_terms($product_id, 'product_cat');
  $category_ids = array();

  if ($terms && !is_wp_error($terms)) {
    foreach ($terms as $term) {
      $category_ids[] = $term->term_id;
    }
  }
  if (count($category_ids) === 0) {
    return;
  }

  if (function_exists('rwmb_meta')) {
    $category_id = $category_ids[0];
    $value = rwmb_meta('hscode_text', ['object_type' => 'term'], $category_id);
    if ($value) {
      ob_start(); ?>
      <dl class="meta">
        <dt class="hscode">HSCode:</dt>
        <dd class="hscode" style="margin-left: -1px;">
          <?php echo $value; ?>
        </dd>
      </dl>
      <?php
      echo ob_get_clean();
    }
  }
}, 999, 3);

// 사용자 리스트 페이지에 사용자의 국가를 보이도록 하고
// 국가별 필터/정렬 기능을 추가합니다
// 국가별 필터/정렬 기능 추가 코드 - 시작
function apmmust_get_user_country($country_code)
{
  $countries = WC()->countries->get_countries();
  if (!isset($countries[$country_code])) {
    return '';
  }
  return $countries[$country_code];
}

function apmmust_get_all_countries()
{
  global $wpdb;
  $users = $wpdb->get_col("SELECT ID FROM $wpdb->users");

  $countries = array();
  foreach ($users as $user_id) {
    $country = get_user_meta($user_id, 'billing_country', true);
    if (!empty($country) && !in_array($country, $countries)) {
      $countries[] = $country;
    }
  }
  sort($countries);
  return $countries;
}

add_filter('manage_users_columns', 'apmmust_add_country_column');
function apmmust_add_country_column($columns)
{
  $columns['billing_country'] = 'Country';
  return $columns;
}

add_action('manage_users_custom_column', 'apmmust_show_country_data', 10, 3);
function apmmust_show_country_data($value, $column_name, $user_id)
{
  if ($column_name === 'billing_country') {
    $country_code = get_user_meta($user_id, 'billing_country', true);
    return apmmust_get_user_country($country_code);
  }
  return $value;
}

add_filter('manage_users_sortable_columns', 'apmmust_sortable_billing_country_column');
function apmmust_sortable_billing_country_column($columns)
{
  return wp_parse_args(
    array(
      'billing_country' => 'billing_country'
    ),
    $columns
  );
}

add_action('pre_get_users', 'apmmust_sort_billing_country_column');
function apmmust_sort_billing_country_column($query)
{
  global $pagenow;
  if (!is_admin() || 'users.php' !== $pagenow)
    return $query;

  if (!$query->get('orderby')) {
    return $query;
  }
  $orderby = $query->get('orderby');
  if ($orderby !== 'billing_country')
    return $query;

  $query->set('meta_key', 'billing_country');
  return $query;
}

add_action('restrict_manage_users', 'apmmust_add_filter_by_country_filter');
function apmmust_add_filter_by_country_filter()
{
  $countries = apmmust_get_all_countries();
  $country_map = WC()->countries->get_countries();

  if (isset($_GET['filter_by_country'])) {
    $section = $_GET['filter_by_country'];
    $section = !empty($section[0]) ? $section[0] : $section[1];
  } else {
    $section = -1;
  }

  echo ' <select name="filter_by_country[]" style="float:none;"><option value="">' . esc_html__('Country Filter', 'apmmust') . '</option>';

  foreach ($countries as $country_code) {
    $country_name = $country_map[$country_code];
    $selected = $country_code === $section ? ' selected="selected"' : '';
    echo '<option value="' . $country_code . '"' . $selected . '>' . $country_name . '</option>';
  }

  echo '</select>';
  echo '<input type="submit" class="button" value="Filter">';

  remove_action('restrict_manage_users', 'apmmust_add_filter_by_country_filter');
}

add_filter('pre_get_users', 'apmmust_filter_users_by_filter_by_country');
function apmmust_filter_users_by_filter_by_country($query)
{
  global $pagenow;
  if (!is_admin() || 'users.php' !== $pagenow)
    return $query;

  $empty = 'yes';
  if (!empty($_GET['filter_by_country'])) {
    foreach ($_GET['filter_by_country'] as $item) {
      if (!empty($item)) {
        $empty = 'no';
      }
    }
  }

  if ($empty === 'yes')
    return $query;

  $country_code = $_GET['filter_by_country'];
  $country_code = !empty($country_code[0]) ? $country_code[0] : $country_code[1];

  $meta_query = array(
    'relation' => 'AND',
    array(
      'key' => 'billing_country',
      'value' => $country_code,
      'compare' => '=='
    ),
  );
  $query->set('meta_query', $meta_query);
  return $query;
}
// 국가별 필터/정렬 기능 추가 코드 - 끝
// 고객 별 고객가입 날짜 조회 Users>All Users
add_filter('manage_users_columns', 'add_registered_date_column');
function add_registered_date_column($columns) {
    $columns['registered_date'] = __('Registered Date', 'woocommerce');
    return $columns;
}

add_action('manage_users_custom_column', 'show_registered_date_column', 10, 3);
function show_registered_date_column($value, $column_name, $user_id) {
    if ($column_name === 'registered_date') {
        $user = get_userdata($user_id);
        $registered_date = $user->user_registered;
        $days_diff = round((time() - strtotime($registered_date)) / (60 * 60 * 24));
        
        if ($days_diff >= 1) {
            return date("Y-m-d", strtotime($registered_date)) . ' / ' . $days_diff . ' days ago';
        } else {
            return date("Y-m-d", strtotime($registered_date));
        }
    }
    return $value;
}

add_filter('manage_users_sortable_columns', 'sortable_registered_date_column');
function sortable_registered_date_column($columns) {
    $columns['registered_date'] = 'registered_date';
    return $columns;
}

add_action('pre_get_users', 'sort_registered_date_column');
function sort_registered_date_column($query) {
    global $pagenow;
    if (!is_admin() || 'users.php' !== $pagenow) {
        return $query;
    }

    if (!$query->get('orderby')) {
        return $query;
    }

    $orderby = $query->get('orderby');
    if ($orderby !== 'registered_date') {
        return $query;
    }

    $query->set('orderby', 'registered');
    return $query;
}

// 고객 별 최근 로그인날짜 조회 Users>All Users
add_action('wp_login', 'smartwp_capture_login_time', 10, 2);
function smartwp_capture_login_time($user_login, $user) {
    update_user_meta($user->ID, 'last_login', time());
}
add_filter('manage_users_columns', 'smartwp_user_last_login_column');
add_filter('manage_users_custom_column', 'smartwp_last_login_column', 10, 3);
function smartwp_user_last_login_column($columns) {
    $columns['last_login'] = 'Last Login';
    return $columns;
}
function smartwp_last_login_column($output, $column_id, $user_id) {
    if ($column_id == 'last_login') {
        $last_login = get_user_meta($user_id, 'last_login', true);
        $date_format = 'M j, Y';
        $hover_date_format = 'F j, Y, g:i a';
        
        if ($last_login) {
            $days_diff = round((time() - $last_login) / (60 * 60 * 24));
            
            if ($days_diff > 0) {
                $output = '<div title="Last login: ' . date($hover_date_format, $last_login) . '">' . $days_diff . ' days ago</div>';
            } else {
                $output = '<div title="Last login: ' . date($hover_date_format, $last_login) . '">Today</div>';
            }
        } else {
            $output = 'No record';
        }
    }
    return $output;
}
add_filter('manage_users_sortable_columns', 'smartwp_sortable_last_login_column');
add_action('pre_get_users', 'smartwp_sort_last_login_column');
function smartwp_sortable_last_login_column($columns) {
    return wp_parse_args(
        array(
            'last_login' => 'last_login'
        ),
        $columns
    );
}
function smartwp_sort_last_login_column($query) {
    if (!is_admin()) {
        return $query;
    }
    $screen = get_current_screen();
    if (isset($screen->base) && $screen->base !== 'users') {
        return $query;
    }
    if (isset($_GET['orderby']) && $_GET['orderby'] == 'last_login') {
        $query->query_vars['meta_key'] = 'last_login';
        $query->query_vars['orderby'] = 'meta_value_num';
    }
    return $query;
}


// 고객 별 최근 주문날짜 조회 Users>All Users
add_filter('manage_users_columns', 'add_last_order_column');
function add_last_order_column($columns) {
    $columns['last_order_date'] = 'Last Order Date';
    return $columns;
}

add_filter('manage_users_custom_column', 'fill_last_order_column', 10, 3);
function fill_last_order_column($value, $column_name, $user_id) {
    if ('last_order_date' == $column_name) {
        $args = array(
            'numberposts' => 1,
            'meta_key' => '_customer_user',
            'meta_value' => $user_id,
            'post_type' => wc_get_order_types(),
            'post_status' => array_keys(wc_get_order_statuses()),
            'orderby' => 'date',
            'order' => 'DESC',
        );
        $last_order = get_posts($args);
        if ($last_order) {
            $last_order_date = date("Y-m-d H:i:s", strtotime($last_order[0]->post_date));
            $days_diff = round((time() - strtotime($last_order_date)) / (60 * 60 * 24));
            
            if ($days_diff >= 90) {
                // If it's been more than 90 days, return the date in red and bold
                return '<strong style="color: red;">' . date("Y-m-d", strtotime($last_order_date)) . ' / ' . $days_diff . ' days ago</strong>';
            } else {
                return date("Y-m-d", strtotime($last_order_date)) . ' / ' . $days_diff . ' days ago';
            }
        } else {
            // No orders yet, return the message in red and bold
            return '<strong style="color: red;">No order yet</strong>';
        }
    }
    return $value;
}

// 'Korean' 컬럼 추가
// add_filter('manage_edit-product_attributes_columns', function( $columns ) {
//     $new_columns = array_slice($columns, 0, 1, true) +
//     array("korean" => "Korean") +
//     array_slice($columns, 1, count($columns)-1, true);
//     return $new_columns;
// });

// Korean 컬럼에 값을 넣기
// add_filter('manage_product_attributes_custom_column', function( $content, $column_name, $term_id ) {
//     switch( $column_name ) {
//         case 'korean' :
//             $value = get_term_meta( $term_id, 'apmmust_vendor_korean_term_name', true );
//             if( $value ) {
//                 return $value;
//             }
//             return '';
//         break;
//     }
// }, 10, 3);


$attribute_term_slugs = [
  'product_cat',
  'product_brand',
  'pa_colour',
  'pa_gender',
  'pa_made-in',
  'pa_season',
  'pa_size',
  'product_tag',
];
foreach ($attribute_term_slugs as $slug) {
  add_action( 'init', function() use ($slug) {
    add_filter( "manage_edit-{$slug}_columns", function( $columns ) {
        $_columns = array(
          'cb' => $columns['cb'],
          'name' => $columns['name'],
          'korean_name' => 'K-Name',
        );
        unset($columns['cb']);
        unset($columns['name']);
        $new_columns = array_merge($_columns, $columns);
        return $new_columns;
    });
  });

  add_action( 'init', function() use ($slug) {
    add_filter( "manage_{$slug}_custom_column", function( $content, $column_name, $term_id ) {
        if ( 'korean_name' === $column_name ) {
            $korean_name = rwmb_meta( 'apmmust_vendor_korean_term_name', [ 'object_type' => 'term' ], $term_id );
            $content = empty( $korean_name ) ? '미지정' : $korean_name;
        }
        return $content;
    }, 10, 3 );
  });
}
